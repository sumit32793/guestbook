<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\Message;
use Symfony\Component\HttpFoundation\Response;

class MessageController extends AbstractController
{
    /**
     * @Route("/message", name="message")
     */
    public function index(){
    	$session = new Session();
        if(empty($session->get('access'))){
            return $this->redirectToRoute('index');
        } 
    	// $session = new Session();
    	// echo $session->get('access');
    	// echo $session->get('id');
    	// echo $session->get('username');
    	// echo $session->get('password');
    	// echo $session->get('email');
    	// echo $session->get('fname');
    	// echo $session->get('lname');
    	// echo $session->get('mname');
    	// echo $session->get('mobile');
    	// echo $session->get('created');
    	// echo $session->get('lastlogin');
    	// echo $session->get('status');

    	if (!empty($_POST)) {
    		if (!empty($_POST['message'])) {
    			$entityManager = $this->getDoctrine()->getManager();
    			$message = new Message();
		        $message->setType('text');
		        $message->setUser($session->get('id'));
		        $message->setMessage($_POST['message']);
		        $message->setStatus(0);
		        $message->setCreated(date("Y/m/d H:i:s"));
		        $entityManager->persist($message);
		        $entityManager->flush();
		        return $this->redirectToRoute('dashboard');
    		}
    	}
    	if (!empty($_FILES['messagefile']["name"])) {
    			$target_dir = "uploads/";
    			$ExplodeName = explode(".", $_FILES["messagefile"]["name"]);
    			$FileName = $session->get('id')."-".time().".".end($ExplodeName);
				$target_file = $target_dir . basename($FileName);
				move_uploaded_file($_FILES["messagefile"]["tmp_name"], $target_file);
    			$entityManager = $this->getDoctrine()->getManager();
    			$message = new Message();
		        $message->setType('image');
		        $message->setUser($session->get('id'));
		        $message->setMessage($target_file);
		        $message->setStatus(0);
		        $message->setCreated(date("Y/m/d H:i:s"));
		        $entityManager->persist($message);
		        $entityManager->flush();
		        return $this->redirectToRoute('dashboard');
    	}

    	$em = $this->getDoctrine()->getEntityManager();
        $query = $em->createQuery("SELECT u FROM App\Entity\Message u ");
        $Message = $query->getResult();
        //echo "<pre>"; print_r($Message); echo "</pre>"; exit();
        return $this->render('message/index.html.twig', [
            'User' => $session->get('fname')." ".$session->get('lname'),
            'Access' => $session->get('access'),
            'Message' => $Message,
        ]);
    }

    public function getuserbyid($userid=''){
    	$em = $this->getDoctrine()->getEntityManager();
    	$query = $em->createQuery("SELECT u.fname,u.lname FROM App\Entity\User u WHERE u.id = '$userid'");
        $User = $query->getResult();
        return new Response($User[0]['fname']." ".$User[0]['lname']);
    }

    public function changestatus($messageid='',$status=''){
    	$entityManager = $this->getDoctrine()->getManager();
    	$message = $entityManager->getRepository(Message::class)->find($messageid);
    	if($status == 0){ $message->setStatus(1); }
    	if($status == 1){ $message->setStatus(0); }
    	$entityManager->flush();
    	return $this->redirectToRoute('dashboard');
    }

    public function deletemessage($messageid=''){
    	$entityManager = $this->getDoctrine()->getManager();
    	$message = $entityManager->getRepository(Message::class)->find($messageid);
    	$entityManager->remove($message);
    	$entityManager->flush();
    	return $this->redirectToRoute('dashboard');
    }

    public function updatemessage(){
    	//echo "<pre>"; print_r($_POST); echo "</pre>"; 
    	//echo "<pre>"; print_r($_FILES); echo "</pre>"; exit();
    	$session = new Session();
    	if(!empty($_POST['messageid'])){ $messageid = $_POST['messageid']; }
    	$entityManager = $this->getDoctrine()->getManager();
    	$message = $entityManager->getRepository(Message::class)->find($messageid);
    	if (!empty($_POST)) {
    		if (!empty($_POST['message'])) {
		        $message->setType('text');
		        $message->setMessage($_POST['message']);
    		}
    	}
    	if (!empty($_FILES['messagefile']["name"])) {
    			$target_dir = "uploads/";
    			$ExplodeName = explode(".", $_FILES["messagefile"]["name"]);
    			$FileName = $session->get('id')."-".time().".".end($ExplodeName);
				$target_file = $target_dir . basename($FileName);
				move_uploaded_file($_FILES["messagefile"]["tmp_name"], $target_file);
		        $message->setType('image');
		        $message->setMessage($target_file);
    	}
    	$entityManager->flush();
    	return $this->redirectToRoute('dashboard');
    }

    public function logout(){
    	session_destroy();
		return $this->redirectToRoute('index');
    }
}
